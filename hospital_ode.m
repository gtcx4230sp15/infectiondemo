% Challenge 21.1

% I(T) = Y(:, 1)
% S(T) = Y(:, 2)
% R(T) = Y(:, 3)

[T1, Y1] = ode23s (@ode_model1, [0 40], [.01 .99 0]);

if true,
    figure (1); clf;
    plot (T1, Y1, '*-'); legend ('I(t)', 'S(t)', 'R(t)', 0);
    grid on;
    xlabel ('t');
    ylabel ('Proportion of population');
end

fp = fopen ('ode_model1.csv', 'w');
fprintf (fp, 'T,I,S,R\n');
fprintf (fp, '%f,%f,%f,%f\n', [T1 Y1]');
fclose (fp);

sol3 = dde23 (@dde_model3, [4], @init_model3, [0 40]);

if true,
    figure (2); clf;
    plot (sol3.x', sol3.y', '*-'); legend ('I(t)', 'S(t)', 'R(t)', 0);
    grid on;
    xlabel ('t');
    ylabel ('Proportion of population');
end

fp = fopen ('dde_model3.csv', 'w');
fprintf (fp, 'T,I,S,R\n');
fprintf (fp, '%f,%f,%f,%f\n', [sol3.x' sol3.y']');
fclose (fp);

% eof
