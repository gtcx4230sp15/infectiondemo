function [NewPatients] = hospital_sim_one (Patients, k, tau, delta, nu)
% hospital_sim_one: Simulates one time-step of CA disease model.
%
% NewState = hospital_sim_one (State, k, tau, delta, nu);
%
% Arguments:
%   State - Patient state (see below)
%   k - Length of infection, in time steps
%   tau - Probability of contraction
%   delta - Probability of swapping
%   nu - Probability of vaccination

[m, n] = size (Patients);

% Who is next to an infected patient?
% Checks below, above, left, right
NextToSick = zeros (m, n);
if m >= 2,
  NextToSick(1:m-1, :) = NextToSick(1:m-1, :) | (Patients(2:m, :) >= 1);
  NextToSick(2:m, :) = NextToSick(2:m, :) | (Patients(1:m-1, :) >= 1);
end
if n >= 2,
  NextToSick(:, 2:n) = NextToSick(:, 2:n) | (Patients(:, 1:n-1) >= 1);
  NextToSick(:, 1:n-1) = NextToSick(:, 1:n-1) | (Patients(:, 2:n) >= 1);
end

% First, we vaccinate susceptible patients
Luck = rand (m, n);
Vaccinate = (Patients == 0) & (Luck >= (1-nu));
Patients = Patients + (Vaccinate*(-2));

% New infections occur when
% - patient is susceptible
% - patient is next to a sick patient
% - patient is unlucky
Luck = rand (m, n);
NewInfections = (Patients == 0) ...
    & NextToSick ...
    & (Luck <= tau);

% Currently sick will advance
Advance = (Patients >= 1) & (Patients < k);

% Infection ends after k days
Recovered = (Patients == k);

% Now, update patient state
NewPatients = Patients + NewInfections + Advance - Recovered*(k+1);

% Randomly swap patients. This swaps patients one at a time, which is
% not terribly efficient.
[Iswap, Jswap] = find (rand (m, n) <= delta);
for ij_swap = 1:length(Iswap),
    IJnew = ceil (rand (1, 2) .* [m, n]);
    temp = NewPatients(IJnew(1), IJnew(2));
    NewPatients(IJnew(1), IJnew(2)) = NewPatients(Iswap(ij_swap), Jswap(ij_swap));
    NewPatients(Iswap(ij_swap), Jswap(ij_swap)) = temp;
end

% eof
