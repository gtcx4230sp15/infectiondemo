function dy = ode_model1 (t, y)
% ode_model1: O'Leary, Challenge 21.1

tau = 0.8;
k = 4;

dy = [tau*y(1)*y(2) - y(1)/k ; ...
      -tau*y(1)*y(2) ; ...
      y(1)/k ; ...
      ];

% eof
