# Infection Demo [CX 4230, Spring 2015] #

These demos accompany the lecture on the diffusion equation. They are based on Chapters 19 and 21 of Dianne P. O'Leary's excellent text, _[Scientific computing with case studies](http://www.cs.umd.edu/~oleary/SCCS/)_, SIAM Press, 2009.
