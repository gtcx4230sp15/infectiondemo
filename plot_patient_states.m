function h = plot_patient_states (Patients, StateRange)
% plot_patient_states: Plots the condition of 2-D grid of patients.
%
% h = plot_patient_states (Patients, k);
%
% Arguments:
%   Patients - States (m x n grid)
%   StateRange - [min, max] values (2-vector)
%
% Returns h, a handle to the figure.
%

h = imagesc (Patients, StateRange);
colorbar;

% eof
