% Challenge 21.4, which is 21.1 with spatial variation

do_run = 1;
do_movie = 1;
do_save_movie = 0; save_movie_suffix = '--11x11--tau-0.8--delta-0.2--k-4';
do_save_data = 0;
FRAMEREP = 5;

if do_run,
  m = 11;
  n = 11;
  Tlim = [0 40];

  I0 = zeros (m, n);
  I0(ceil (m/2), ceil (n/2)) = 0.5;
  S0 = ones (m, n) - I0;

  Y0 = [reshape(I0, m*n, 1) ; ...
        reshape(S0, m*n, 1) ; ...
       ];

  % I(T) = Y(:, 1)
  % S(T) = Y(:, 2)

  display (sprintf ('Running solver...'));
  [T1, Y1] = ode23s (@pde_model1, Tlim, Y0);
  display (sprintf ('... done!'));

  CIt = zeros (length (T1), 1);
  CSt = zeros (length (T1), 1);

  % Extract summary statistics
  for i=1:length (T1),
    It = reshape (Y1(i, 1:(m*n)), m, n);
    St = reshape (Y1(i, (m*n+1):(2*m*n)), m, n);
    Rt = 1 - It - St;

    % Integrate results over the domain to get total populations
    CIt(i) = sum (sum (It)) / m / n;
    CSt(i) = sum (sum (St)) / m / n;
    CRt(i) = sum (sum (Rt)) / m / n;
  end
end

if do_movie,
  clear Ms;
  clear Ms2;

  Ms = [];
  Ms2 = [];

  for i=1:length (T1),
    It = reshape (Y1(i, 1:(m*n)), m, n);
    St = reshape (Y1(i, (m*n+1):(2*m*n)), m, n);
    Rt = 1 - It - St;

    figure (1); clf;
    pcolor (It);
    colorbar;
    %    caxis ([-1 1]);
    title (sprintf ('t=%f: %.3f recovered', T1(i), sum (sum (Rt))/m/n));

    for di=1:FRAMEREP,
      if (i==1) & (di==1),
        Ms = getframe (gcf);
      else
        Ms((i-1)*FRAMEREP + di) = getframe (gcf);
      end
    end

    figure (2); clf;
    plot (T1(1:i), CSt(1:i), '--', T1(1:i), CIt(1:i), '*-', T1(1:i), ...
          CRt(1:i), '-');
    axis ([Tlim 0 1]);
    grid on;
    legend ('S(t)', 'I(t)', 'R(t)', 'Location', 'BestOutside');

    for di=1:FRAMEREP,
      if (i==1) & (di==1),
        Ms2 = getframe (gcf);
      else
        Ms2((i-1)*FRAMEREP + di) = getframe (gcf);
      end
    end
  end

  if do_save_movie,
      v = VideoWriter (sprintf ('I%s.mp4', save_movie_suffix), ...
                       'MPEG-4')
      open (v)
      writeVideo (v, Ms)
      close (v)

      v = VideoWriter (sprintf ('Curves%s.mp4', save_movie_suffix), ...
                       'MPEG-4')
      open (v)
      writeVideo (v, Ms2)
      close (v)
  end
end

if false,
if true,
    figure (1); clf;
    plot (T1, Y1, '*-'); legend ('I(t)', 'S(t)', 'R(t)', 0);
    grid on;
    xlabel ('t');
    ylabel ('Proportion of population');
end

if do_save_data,
    fp = fopen ('pde_model1.csv', 'w');
    fprintf (fp, 'T,I,S,R\n');
    fprintf (fp, '%f,%f,%f,%f\n', [T1 Y1]');
    fclose (fp);
end
end

% eof
