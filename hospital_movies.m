FRAMEREP = 5;

for STUDY=1:2,
%for STUDY=3:3,
    if STUDY >= 1,
        m = 10;
        n = m;
        k = 4;
        tau = 0.2;
        delta = 0;
        nu = 0;
    end
    if STUDY >= 2,
        delta = 0.01;
    end
    if STUDY >= 3,
        nu = 0.1;
    end

    for trial=1:5,
        do_movie = 1;
        hospital_mc;

        % Create "slow" versions of the movie before writing
        clear Ms;
        clear M2s;

        for tstep=1:t,
            for frame=1:FRAMEREP,
                Ms((tstep-1)*FRAMEREP + frame) = M(tstep);
                M2s((tstep-1)*FRAMEREP + frame) = M2(tstep);
            end
        end

        mpgwrite (Ms, colormap, sprintf ('ward-%d-%d.mpg', STUDY, trial));
        mpgwrite (M2s, colormap, sprintf ('state-%d-%d.mpg', STUDY, trial));
    end
end

% eof
