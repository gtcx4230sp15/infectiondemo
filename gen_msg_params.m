function msg = gen_msg_params (m, n, k, tau, delta, nu)
% gen_msg_params: Generates a text string parameter summary.
%
% msg = gen_msg_params (m, n, k, tau, delta, nu)
%
% Arguments:
%   m x n - Grid size
%   k - Length of infection
%   tau - Probability of infection
%   delta - Probability of swapping
%   nu - Probability of vaccination
%
% Notes: If nu <= 0, then it is omitted; if delta <= 0, both are.
%
% Returns:
%   msg - Text string
%

if delta > 0,
    if nu > 0,
        msg = sprintf ('%d x %d: k=%d, \\tau=%.3f, \\delta=%.3f, \\nu=%.3f', ...
                       m, n, k, tau, delta, nu);
    else
        msg = sprintf ('%d x %d: k=%d, \\tau=%.3f, \\delta=%.3f', ...
                       m, n, k, tau, delta);
    end
else
    msg = sprintf ('%d x %d: k=%d, \\tau=%.3f', m, n, k, tau);
end

% eof
