function y = init_model3 (t)
% init_model3: Initial values for O'Leary, Challenge 21.3

if t < 0,
  y = [0 1 0]';
else
  y = [0.01 0.99 0]';
end

% eof
