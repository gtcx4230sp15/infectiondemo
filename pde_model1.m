function dY = pde_model1 (t, Y)
% pde_model1: O'Leary, Challenge 21.4 / 21.1+PDE

tau = 0.8;
C = 0.2;
k = 4;
m = 11;
n = 11;

hx = 1 / (m-1);
hy = 1 / (n-1);

I0 = reshape (Y(1:m*n), m, n);
S0 = reshape (Y((m*n+1):(2*m*n)), m, n);

delI0x = -2*I0;
if m >= 2,
  delI0x(1:m-1, :) = delI0x(1:m-1, :) + I0(2:m, :);
  delI0x(2:m, :) = delI0x(2:m, :) + I0(1:m-1, :);

  % Enforce Neumann boundary conditions:
  delI0x(1, :) = delI0x(1, :) + I0(2, :);
  delI0x(m, :) = delI0x(m, :) + I0(m-1, :);
end
delI0y = -2*I0;
if n >= 2,
  delI0y(:, 1:n-1) = delI0y(:, 1:n-1) + I0(:,2:n);
  delI0y(:, 2:n) = delI0y(:, 2:n) + I0(:, 1:n-1);

  % Enforce Neumann boundary conditions:
  delI0y(:, 1) = delI0y(:, 1) + I0(:, 2);
  delI0y(:, n) = delI0y(:, n) + I0(:, n-1);
end
delI0xy = (delI0x / (hx^2)) + (delI0y / (hy^2));

dI0 = (tau*I0.*S0) - (I0/k) + (C*delI0xy.*S0);
dS0 = (-tau*I0.*S0) - (C*delI0xy.*S0);

dY = [reshape(dI0, m*n, 1) ; ...
      reshape(dS0, m*n, 1) ; ...
     ];

% eof
