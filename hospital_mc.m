% Example from Dianne O'Leary's book
%
% Caller of this script must define:
%   m x n := Dimension of bed grid
%   k := Length of infection, in days
%   tau := Probability of contraction
%   delta := Probability of swapping
%   nu := Probability of vaccination
%

%================================================
% Defines input parameters.
%================================================

if ~exist ('do_movie'),
    do_movie = 0;
end

%================================================
% Checks input parameters.
%
% Code doesn't work if the parameters do not meet
% the following conditions.
%================================================

assert (m >= 1);
assert (n >= 1);
assert (k >= 1);
assert (tau >= 0);
assert (tau <= 1);
assert (delta >= 0);
assert (delta <= 1);
assert (nu >= 0);
assert (nu <= 1);

%================================================
% Patient states:
%            -2 : Vaccinated
%            -1 : Recovered
%             0 : Susceptible
%   i in [1, k] : In day 'i' of the infection
%================================================

% Initially, none are infected but everyone susceptible
Patients = zeros (m, n);

% Infect the middle patient
Patients(ceil (m/2), ceil (n/2)) = 1;

%================================================
% Main simulation loop
%================================================

% Track number of infected, susceptible, and recovered patients.
NumInfected = [0];
NumRecovered = [0];
NumVaccinated = [0];

t = 1;
NumInfected(t) = (sum (sum (Patients >= 1)));
NumSusceptible = m*n - NumInfected - NumRecovered - NumVaccinated;


if do_movie,
    figure (1); clf;
    h = plot_patient_states (Patients, [-2 k]);
    h_t = title ([gen_msg_sumstats(t, ...
                                   NumSusceptible(t)/m/n, ...
                                   NumInfected(t)/m/n, ...
                                   NumRecovered(t)/m/n, ...
                                   NumVaccinated(t)/m/n, nu > 0), ...
                  ' -- ', ...
                  gen_msg_params(m, n, k, tau, delta, nu)]);
    set (gca, 'FontSize', 12);
    set (h_t, 'FontSize', 12);
    M = getframe(gcf);

    figure (2); clf;
    h = plot_patient_sumstats (1:t, ...
                               NumSusceptible/m/n, ...
                               NumRecovered/m/n, ...
                               NumInfected/m/n, ...
                               NumVaccinated/m/n);
    h_t = title (gen_msg_params (m, n, k, tau, delta, nu));
    h_x = xlabel ('t');
    h_y = ylabel ('Proportion of Population');
    set (gca, 'FontSize', 12);
    set (h_t, 'FontSize', 12);
    set (h_x, 'FontSize', 12);
    set (h_y, 'FontSize', 12);
    M2 = getframe(gcf);
end

while NumInfected(t) >= 1,
    Patients = hospital_sim_one (Patients, k, tau, delta, nu);

    % Update time-step and key stats
    t = t + 1;
    num_infected = sum (sum (Patients >= 1));
    num_susceptible = sum (sum (Patients == 0));
    num_recovered = sum (sum (Patients == -1));
    num_vaccinated = sum (sum (Patients == -2));

    % Perform sanity check on computed values
    assert ((num_recovered + num_vaccinated) == ((m*n) - num_infected - num_susceptible));

    NumInfected(t) = num_infected;
    NumSusceptible(t) = num_susceptible;
    NumRecovered(t) = num_recovered;
    NumVaccinated(t) = num_vaccinated;

    if do_movie,
        figure (1); clf;
        h = plot_patient_states (Patients, [-2 k]);
        h_t = title ([gen_msg_sumstats(t, ...
                                       NumSusceptible(t)/m/n, ...
                                       NumInfected(t)/m/n, ...
                                       NumRecovered(t)/m/n, ...
                                       NumVaccinated(t)/m/n, nu > 0), ...
                      ' -- ', ...
                      gen_msg_params(m, n, k, tau, delta, nu)]);
        set (gca, 'FontSize', 12);
        set (h_t, 'FontSize', 12);
        M(t) = getframe (gcf);
        
        figure (2); clf;
        h = plot_patient_sumstats (1:t, ...
                                   NumSusceptible'/m/n, ...
                                   NumInfected'/m/n, ...
                                   NumRecovered'/m/n, ...
                                   NumVaccinated'/m/n);
        h_t = title (gen_msg_params (m, n, k, tau, delta, nu));
        h_x = xlabel ('t');
        h_y = ylabel ('Proportion of Population');
        set (gca, 'FontSize', 12);
        set (h_t, 'FontSize', 12);
        set (h_x, 'FontSize', 12);
        set (h_y, 'FontSize', 12);
        M2(t) = getframe (gcf);
    end
end

% eof
