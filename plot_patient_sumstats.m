function [h, h_leg] = plot_patient_sumstats (T, S, I, R, V)
% plot_patient_sumstats: Plots summary statistics for single run.
%
% [h, h_leg] = plot_patient_sumstats (T, S, I, R, V);
%
% Arguments:
%   T - Time values
%   S - Proportion susceptible over time
%   I - Proportion infected over time
%   R - Proportion recovered over time
%   V - Proportion vaccinated over time
%
% Notes:
%   * The vector inputs must be column vectors.
%   * To disable display of vaccinated patients, set V to 0.
%
% Returns:
%   h - Handle to the figure
%   h_leg - Handle to the figure legend
%

if V == 0,
  h = plot (T, S, '--', T, I, '*-', T, R, '-');
  grid on;
  h_leg = legend ('S(t)', 'I(t)', 'R(t)', 'Location', 'BestOutside');
else
  h = plot (T, S, '--', T, I, '*-', T, R, '-', T, V, 's-.');
  grid on;
  h_leg = legend ('S(t)', 'I(t)', 'R(t)', 'V(t)', 'Location', 'BestOutside');
end

% eof
