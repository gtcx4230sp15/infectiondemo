function msg = gen_msg_sumstats (t, s, i, r, v, display_v)
% gen_msg_sumstats: Generates a text string time-step summary.
%
% msg = gen_msg_sumstats (t, s, i, r, v, display_v);
%
% Arguments:
%   t - Time step ID
%   s - No. susceptible at t
%   i - No. infected at t
%   r - No. recovered at t
%   v - No. vaccinated at t
%   display_v - If yes, include v; otherwise, omit
%
% Returns:
%   msg - Text string
%

if display_v,
  msg = sprintf ('t=%d: S(t)=%.3f, I(t)=%.3f, R(t)=%.3f, V(t)=%.3f', ...
                 t, s, i, r, v);
else
  msg = sprintf ('t=%d: S(t)=%.3f, I(t)=%.3f, R(t)=%.3f', ...
                 t, s, i, r);
end

% eof
