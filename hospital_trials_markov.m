num_trials = 10000;
do_movie = 0;

for STUDY=1:1,
    display (sprintf ('STUDY=%d: Running %d trials...', STUDY, ...
                      num_trials));

    if STUDY >= 1,
        m = 1;
        n = 3;
        k = 2;
        tau = 0.2;
        delta = 0;
        nu = 0;
    end
    if STUDY >= 2,
        delta = 0.01;
    end
    if STUDY >= 3,
        nu = 0.1;
    end

    StatDays = zeros (num_trials, 1);
    StatRecovered = zeros (num_trials, 1);
    StatSusceptible = zeros (num_trials, 1);
    StatVaccinated = zeros (num_trials, 1);
    for trial = 1:num_trials,
        t = 0;
        hospital_mc;
        StatDays(trial) = t;
        StatRecovered(trial) = NumRecovered(t);
        StatSusceptible(trial) = NumSusceptible(t);
        StatsVaccinated(trial) = NumVaccinated(t);

        if (mod (trial-1, ceil (0.1*num_trials)) == 0) | (trial == num_trials),
            display (sprintf ('Trial #%d: %d days, R(%d)=%d, S(%d)=%d, V(%d)=%d', ...
                              trial, StatDays(trial), ...
                              StatDays(trial), StatRecovered(trial), ...
                              StatDays(trial), StatSusceptible(trial), ...
                              StatDays(trial), StatVaccinated(trial)));
        end
    end

    out_trials = sprintf ('trials%d.csv', STUDY);
    fp = fopen (out_trials, 'w');
    fprintf (fp, 'Days,Recovered,Uninfected,Vaccinated\n');
    AllStats = [StatDays StatRecovered StatSusceptible StatVaccinated];
    fprintf (fp, '%d,%d,%d,%d\n', AllStats');
    fclose (fp);
end

if (m == 1) & (n == 3),
    Q = StatRecovered == 1;
    PS = StatRecovered == 2;
    R = StatRecovered == 3;

    T = (1:num_trials)';
    Q_mu = cumsum (Q) ./ T;
    PS_mu = cumsum (PS) ./ T;
    R_mu = cumsum (R) ./ T;

    loglog (T, abs([Q_mu-0.41 PS_mu-0.46 R_mu-0.13]));
    grid on;

    fp = fopen ('markov_error.csv', 'w');
    fprintf (fp, 'Trial,State,Error\n');
    fprintf (fp, '%d,Q,%f\n', [T Q_mu-0.41]');
    fprintf (fp, '%d,P&S,%f\n', [T PS_mu-0.46]');
    fprintf (fp, '%d,R,%f\n', [T R_mu-0.13]');
    fclose (fp);
end

% eof
