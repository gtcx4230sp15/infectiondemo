function dy = dde_model3 (t, y, y_lag)
% dde_model3: O'Leary, Challenge 21.3

tau = 0.8;
k = 4;

I_t = y(1);
S_t = y(2);
R_t = y(3);

I_t_lag = y_lag(1);
S_t_lag = y_lag(2);

dy = [tau*(I_t*S_t - I_t_lag*S_t_lag) ; ...
      -tau*I_t*S_t ; ...
      tau*I_t_lag*S_t_lag ; ...
     ];

% eof
